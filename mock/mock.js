const random_jokes = [
    {
        setup: 'What is the object oriented way to get wealthy ?',
        punchline: 'Inheritance',
    },
    {
        setup: 'To understand what recursion is...',
        punchline: "You must first understand what recursion is",
    },
    {
        setup: 'What do you call a factory that sells passable products?',
        punchline: 'A satisfactory',
    },
];

let cardsList = [
    {
        id: 1,
        name: '百度',
        desc: 'Did you hear about the two silk worms in a race?',
        url: 'http://www.baidu.com',
    },{
        id: 2,
        name: '腾讯',
        desc: 'Did you hear about the two silk worms in a race?',
        url: 'http://www.qq.com',
    },{
        id: 3,
        name: '阿里',
        desc: 'Did you hear about the two silk worms in a race?',
        url: 'http://www.aliyun.com',
    },

        ];
let random_joke_call_count = 0;


export default {
    'get /dev/random_joke': function (req, res) {
        // res.status(500);
        // res.json({});
        const responseObj = random_jokes[random_joke_call_count % random_jokes.length];
        random_joke_call_count += 1;
        setTimeout(() => {
            res.json(responseObj);
        }, 3000);
    },

    // 'get /api/cards':function (req,res) {
    //
    //     setTimeout(() => {
    //         res.json({data:cardsList});
    //     }, 800);
    // },

    'delete /api/cards/:id':function (req, rsp, next) {
        cardsList = cardsList.filter(v=>v.id!==parseInt(req.params.id));
        setTimeout(()=>{
            rsp.json({success:true})
        },300)
    },

    'post /api/cards/add':function (req,resp,next) {
        cardsList = [...cardsList,{
            ...req.body,
            id:cardsList[cardsList.length-1].id+1
        }];
        resp.json({
            success: true
        });
    },
    'get /api/cards/:id/statistic':function (req, rsp, next) {
        rsp.json({
            result: [
                { genre: 'Sports', sold: 275 },
                { genre: 'Strategy', sold: 1150 },
                { genre: 'Action', sold: 120 },
                { genre: 'Shooter', sold: 350 },
                { genre: 'Other', sold: 150 },
            ]
        });
    }



}