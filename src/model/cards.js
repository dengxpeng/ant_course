import * as cardsService from '../service/cards';
import { message } from 'antd';


const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    });
};

export  default {
    namespaces:'cards',
    state:{
        cardsList:[

        ],
        result:{}
    },
    effects: {
        *queryList(_, { call, put }) {
            try {
                const result = yield call(cardsService.queryList);
                yield put({type: 'save', payload:{cardsList:result.data}});

            }catch (e) {
                message.error("获取数据失败！");
            }
        },
        *deleteOne({payload},{call,put}){
            const rsp = yield call(cardsService.deleteOne,payload);
            if (rsp.success){
                yield put({type:'queryList'});
            }
            return rsp;
        },
        *addOne({payload},{call,put}){
            const rsp = yield call(cardsService.addOne,payload);
            if (rsp.success){
                yield put({type:'queryList'});
            }
            return rsp;
        },
        *getStatisticData({payload},{call,put}){
            const rsp = yield call(cardsService.getStatisticData,payload);
            yield put({
                type:'saveStatisticData',
                payload:{
                    id:payload,
                    data:rsp.result
                }
            });
        }

    },
    reducers:{
        save(state, {payload:{cardsList}}){
           return {
               ...state,
               cardsList
           }
        },
        saveStatisticData(state,{ payload : { id , data } }){
            return {
                ...state,
                result:{
                    ...state.result,
                    [id]:data
                }
            };
        }
    }
}

