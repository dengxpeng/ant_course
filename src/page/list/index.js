import React from 'react';
import { Table, Modal, Button, Form, Input } from 'antd';
import { connect } from 'dva';
import SampleChart from "../../components/SampleChart";
const FormItem = Form.Item;


class List extends React.Component{

    state = {
        visible: false,
        statisticVisible: false,
        id: null,
    };

    componentDidMount() {
        this.props.dispatch({
            type: 'cards/queryList',
        })
    }

    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = ()=>{
        this.setState({
            visible:false,
        })
    }


    handleOk=()=>{
        const {dispatch,form:{validateFields }} = this.props;

        validateFields((err,values)=>{
            if(!err){
                dispatch({
                    type:'cards/addOne',
                    payload:values,
                });
                this.setState({
                    visible:false
                })
            }
        })
    }


    showStatistic = (id)=>{
        const {dispatch} = this.props;
        dispatch({
            type: 'cards/getStatisticData',
            payload: id,
        });
        this.setState({ id, statisticVisible: true });
    }

    handleStatisticCancel = () => {
        this.setState({
            statisticVisible: false,
        });
    }


    columns = [
        {
            title: '名称',
            dataIndex: 'name',
        },
        {
            title: '描述',
            dataIndex: 'desc',
        },
        {
            title: '链接',
            dataIndex: 'url',
            render: value => <a href={value}>{value}</a>,
        },
        {
            title: '图表',
            dataIndex: 'id',
            render: (_, { id }) => {
                return (
                    <Button onClick={() => {this.showStatistic(id); }}>图表</Button>
                );
            },
        },
    ];

    render() {
        const { visible,statisticVisible, id } = this.state;
        const { cardsList, cardsLoading,form:{getFieldDecorator},result } = this.props;
        return (
            <div>
                <Table columns={this.columns} dataSource={cardsList} loading={cardsLoading} rowKey="id" />
                <Button onClick={this.showModal}>新建</Button>
                <Modal title="新建记录" visible={visible} onCancel={this.handleCancel} onOk={this.handleOk}>
                    <Form>
                        <FormItem label={"名称"}>
                            {getFieldDecorator('name',{
                                rules:[{required:true}],
                            })(<Input/>)}
                        </FormItem>
                        <FormItem label={"描述"}>
                            {getFieldDecorator('desc')(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label={"链接"}>
                            {getFieldDecorator('url',{
                                rules:[{type:'url'}],
                            })(<Input/>)}
                        </FormItem>
                    </Form>
                </Modal>

                <Modal visible={statisticVisible} footer={null} onCancel={this.handleStatisticCancel}>
                    <SampleChart data={result[id]} ixx={id} result={result}/>
                </Modal>
            </div>
        )
    }
}

export default connect((state)=>{
    return {
        cardsList: state.cards.cardsList,
        result: state.cards.result,
        cardsLoading: state.loading.effects['cards/queryList'],
    };
})(Form.create()(List));